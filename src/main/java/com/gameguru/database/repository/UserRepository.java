package com.gameguru.database.repository;

import com.gameguru.database.entity.User;
import com.gameguru.database.exception.GameGuruException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class UserRepository {
    private final Connection connection;

    public UserRepository(Connection connection) {
        this.connection = connection;
    }


    //CRUD - Create
    public void create(String firstName, String lastName){
        String sql = "INSERT INTO users (first_name , last_name) VALUES (?,?)";
        try(PreparedStatement createStatement = connection.prepareStatement(sql)){
            createStatement.setString(1, firstName);
            createStatement.setString(2, lastName);
            createStatement.execute();
        }catch(SQLException e){
            throw new GameGuruException(e);
        }
    }

    //CRUD - Update
    public void update(User user){
        String sql = "UPDATE users SET first_name = ? WHERE id =  ?";
        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setString(1, user.getFirstName());
            stmt.setInt(2, user.getId());
            stmt.execute();
        }catch(SQLException e){
            throw new GameGuruException(e);
        }
    }

    //CRUD - Delete
    public boolean delete(int id){
        String sql = "DELETE FROM users WHERE id =  ?";
        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        }catch(SQLException e){
            throw new GameGuruException(e);
        }
    }

    // Crud - read
    public List<User> list(){
        try(Statement stmt = connection.createStatement()){
            String sql = "SELECT * FROM users";
            try(ResultSet rs = stmt.executeQuery(sql)){
                return list(rs);
            }

        }catch(SQLException e){
            throw new GameGuruException(e);
        }
    }

    private List<User> list(ResultSet resultSet){
        List<User> list = new ArrayList<>();
        try{
            while(resultSet.next()){
                list.add(toProductEntity(resultSet));
            }
            return list;
        } catch (SQLException e) {
            throw new GameGuruException(e);
        }

        //---------------------------------------------
    }




    private static User toProductEntity(ResultSet resultSet){
        User entity = new User();
        try {
            entity.setId(resultSet.getInt("id"));
            entity.setFirstName(resultSet.getString("first_name"));
            entity.setLastName(resultSet.getString("last_name"));
            entity.setDateCreated(resultSet.getTimestamp("date_created"));
            entity.setAddressId(resultSet.getInt("address_id"));
        } catch (SQLException e) {
            throw new GameGuruException(e);
        }
        return entity;
    }

    public User getById(int id){
        return findById(id).orElse(null);
    }

    public Optional<User> findById(int id){
        String sql = "SELECT * FROM users WHERE id = ?";
        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setInt(1, id);
            try(ResultSet rs = stmt.executeQuery()){
                if(rs.next()){
                    return Optional.of(toProductEntity(rs));
                }
                return Optional.empty();
            }
        }catch(SQLException e){
            throw new GameGuruException(e);
        }
    }
}
