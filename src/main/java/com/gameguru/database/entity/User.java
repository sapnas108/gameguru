package com.gameguru.database.entity;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Setter
@Getter
public class User {

    private int id;
    private String firstName;
    private String lastName;
    private Timestamp dateCreated;
    private int addressId;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateCreated=" + dateCreated +
                ", addressId=" + addressId +
                '}';
    }
}
