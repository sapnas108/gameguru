package com.gameguru.database.exception;

public class GameGuruException extends RuntimeException{
    public GameGuruException(Throwable cause) {
        super(cause);
    }
}
