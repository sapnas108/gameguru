package com.gameguru;

import com.gameguru.database.entity.User;
import com.gameguru.database.exception.GameGuruException;
import com.gameguru.database.repository.UserRepository;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Main implements AutoCloseable{

    private static final String DB_URL = "jdbc:mysql://78.61.247.130:3306/";
    private static final String DB_SCHEMA = "gameguru";

    private final Connection connection;
    private final UserRepository userRepository;

    public Main() {

        try {
            connection = DriverManager.getConnection(
                    DB_URL + DB_SCHEMA,loadDatabaseProperties());
        } catch (SQLException e) {
            throw new GameGuruException(e);
        }

        this.userRepository = new UserRepository(connection);
    }

    public void run(){
        userRepository.create("Petras","Petraitis");             //Crud - create
        System.out.println("==============================");
        System.out.println(userRepository.getById(3));           //Crud - read
        System.out.println("==============================");
        User entity = userRepository.getById(6);                 //Crud - update
        entity.setFirstName("Marius");
        userRepository.update(entity);
        System.out.println("==============================");
        userRepository.delete(7);                            //Crud - Delete
        System.out.println("==============================");
        userRepository.list().forEach(System.out::println);
    }

    public static void main(String[] args){
        new Main().run();
    }

    public static Properties loadDatabaseProperties(){
        try(InputStream is = ClassLoader.getSystemResourceAsStream("database.properties")){
            Properties properties = new Properties();
            properties.load(is);
            return properties;
        }catch (IOException e){
            throw new GameGuruException(e);
        }
    }

    @Override
    public void close(){
        try {
            connection.close();
        } catch(SQLException e){
            throw new GameGuruException(e);
        }
    }
}
